from two_d_point import TwoDPoint
from typing import Tuple

class Quadrilateral:

    def __init__(self, *floats):
        points = TwoDPoint.from_coordinates(list(floats))
        self.__vertices = tuple(points[0:4])

    def __str__(self):
        output = "Quadrilateral\n "

        # Gathering the points
        for edge in self.vertices:
            output = output+str(edge) + ","

        output = output + "\n Smallest-x: " + str(self.smallest_x())

        return output

    def __eq__(self, other):

        # We only compare object that are Quadrilateral
        if type(self) is not type(other):
            return False

        this_vert = self.vertices
        that_vert = other.vertices

        for i_vert in this_vert:
            if i_vert not in that_vert:
                return False

        return True

    def __ne__(self, other: object) -> bool:
        return not self.__eq__(other)

    @property
    def vertices(self):
        return self.__vertices

    def side_lengths(self):
        """Returns a tuple of four floats, each denoting the length of a side of this quadrilateral. The value must be
        ordered clockwise, starting from the top left corner."""
        pointA = self.vertices[0]
        pointB = self.vertices[1]
        pointC = self.vertices[2]
        pointD = self.vertices[3]

        list_lengths = []

        ab = TwoDPoint.length(pointA, pointB)
        bc = TwoDPoint.length(pointB, pointC)
        cd = TwoDPoint.length(pointC, pointD)
        da = TwoDPoint.length(pointD, pointA)

        # Round the god damn points
        round_ab = round(ab*1000)/1000
        round_bc = round(bc*1000)/1000
        round_cd = round(cd*1000)/1000
        round_da = round(da*1000)/1000

        list_lengths.append(round_ab)
        list_lengths.append(round_bc)
        list_lengths.append(round_cd)
        list_lengths.append(round_da)

        output = tuple(list_lengths)

        return output

    def smallest_x(self):
        """Returns the x-coordinate of the vertex with the smallest x-value of the four vertices of this
        quadrilateral."""

        # We assume the smallest is the first vertice's x value until it is proven
        smallest = self.vertices[0].x

        for twodp in self.vertices:
            # This means that the vertices that we are on right now have a smaller x value
            # Therefore we have to replace smallest with it
            if twodp.x < smallest:
                # Replacing smallest with this vertice's x value
                smallest = twodp.x

        # Now if we are out of the for loop we know that smallest have the smallest x value from all of
        # the vertices therefore we can just return it
        return smallest

