from rectangle import Rectangle
from quadrilateral import Quadrilateral
from two_d_point import TwoDPoint
import math

class Square(Rectangle):

    def __init__(self, *floats):
        super().__init__(*floats)
        if not self.__is_member():
            raise TypeError("A square cannot be formed by the given coordinates.")

    def __str__(self):
        output = "Square\n "

        for edge in self.vertices:
            output = output + str(edge) + ","

        output = output + "\n Smallest-x: " + str(self.smallest_x())
        output = output + "\n Area: " + str(self.area())
        output = output + "\n Center: " + str(self.center())

        return output

    def __eq__(self, other):
        if type(self) is not type(other):
            return False

        this_vert = self.vertices
        that_vert = other.vertices

        for i_vert in this_vert:
            if i_vert not in that_vert:
                return False

        return True

    def __ne__(self, other: object) -> bool:
        return not self.__eq__(other)


    def __is_member(self) -> bool:

        # If we are out here then that means it is at least a rectangle then we check further
        # By checking each of the side lengths
        lengths = self.side_lengths()

        # We will be using the first length to compare it to the rest
        comparing = lengths[0]

        for length in lengths:

            # This means that there is a side length that is not equal to the rest
            # Hence it is not a square
            if comparing != length:
                return False

        # If we are out here then that means it is a rectangle with all the side lengths equal which means it is
        # a proper square
        return True

    def snap(self):
        """Snaps the sides of the square such that each corner (x,y) is modified to be a corner (x',y') where x' is the
        integer value closest to x and y' is the integer value closest to y. This, of course, may change the shape to a
        general quadrilateral, hence the return type. The only exception is when the square is positioned in a way where
        this approximation will lead it to vanish into a single point. In that case, a call to snap() will not modify
        this square in any way."""
        vertices = self.vertices

        # This list will be storing the new rounded points
        rounded_points = []

        for edge in vertices:
            # Getting the x and y value of the edge
            x_value = edge.x
            y_value = edge.y

            # Then we just have to round it to the nearest integer
            rounded_x = round(x_value)
            rounded_y = round(y_value)

            # Construct the new rounded point
            rounded_point = TwoDPoint(rounded_x, rounded_y)

            # Then we just append it to the rounded points
            rounded_points.append(rounded_point)

        # If we are outside then we make the output, but since we can only input in different number of floats
        # We have to manually make another list that consist of x and y points
        points = []
        for vert in rounded_points:
            points.append(vert.x)
            points.append(vert.y)

        output = Quadrilateral(*points)

        # However we can't return it uet because we have to check whether or not
        # If the points all collapsed into a single point

        # Getting one of the point from the rounded points so we can check it against
        # With the other points
        a = rounded_points[0]

        for i in range(1, len(rounded_points)):
            # Getting the TwoDPoint at i
            point_i = rounded_points[i]
            
            # Then we check if this is true then it didn't converge to one point
            if (a.x != point_i.x) or (a.y != point_i.y):
                return output
        
        # If we are out here then that means that all of the point collapsed onto one point
        # Thus we return the original square
        return self




