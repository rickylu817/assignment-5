from quadrilateral import Quadrilateral
from two_d_point import TwoDPoint

class Rectangle(Quadrilateral):

    def __init__(self, *floats):
        super().__init__(*floats)
        if not self.__is_member():
            raise TypeError("A rectangle cannot be formed by the given coordinates.")

    def __str__(self):
        output = "Rectangle\n "

        for edge in self.vertices:
            output = output+str(edge) + ","

        output = output + "\n Smallest-x: " + str(self.smallest_x())
        output = output + "\n Area: "+str(self.area())
        output = output + "\n Center: "+str(self.center())

        return output

    def __eq__(self, other):
        if type(self) is not type(other):
            return False

        this_vert = self.vertices
        that_vert = other.vertices

        for i_vert in this_vert:
            if i_vert not in that_vert:
                return False

        return True

    def __ne__(self, other: object) -> bool:
        return not self.__eq__(other)


    def __is_member(self):
        """Returns True if the given coordinates form a valid rectangle, and False otherwise."""
        # This means that the given vertices doesn't have 4 points thus we return false
        if len(self.vertices) != 4:
            return False
        # If we are out here that means we have 4 points and we must check whether the
        # Two diagonals intersect or not
        a = self.vertices[0]
        b = self.vertices[1]
        c = self.vertices[2]
        d = self.vertices[3]

        # Finding the minor diagonal's middle
        acx = (a.x+c.x)/2
        acy = (a.y+c.y)/2

        # Finding the major diagonal's middle
        bdx = (b.x+d.x)/2
        bdy = (b.y+d.y)/2

        # We check if the diagonals intersect, hence it is quadrilateral
        if (acx == bdx) and (acy == bdy):
            # This means that the two diagonals also have the same length
            # Then we know for sure it is an rectangle
            if TwoDPoint.length(a,c) == TwoDPoint.length(b,d):
                return True
            # If it is not the same then it is not a rectangle
            else:
                return False
        # The diagonals don't intersect hence not a rectangle for sure
        else:
            return False

    def center(self):
        """Returns the center of this rectangle, calculated to be the point of intersection of its diagonals."""
        # First we need two opposite points to figure out where the diagonals intersect
        a = self.vertices[0]
        c = self.vertices[2]

        # Finding the mid point between those two opposite points
        output_x = (a.x+c.x)/2
        output_y = (a.y+c.y)/2

        # Rounding it to the nearest 3 digits first before input it into the output
        round_x = round(output_x*1000)/1000
        round_y = round(output_y*1000)/1000

        # Making the center TwoDPoint object
        output = TwoDPoint(round_x, round_y)

        return output

    def area(self):
        """Returns the area of this rectangle. The implementation invokes the side_lengths() method from the superclass,
        and computes the product of this rectangle's length and width."""
        # First get the side lengths by using the quadrilateral's method
        lengths = self.side_lengths()

        # Multiplying the width and the height
        output = lengths[0]*lengths[1]

        round_output = round(output*1000)/1000

        # Returning it
        return round_output
