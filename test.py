from two_d_point import TwoDPoint
from quadrilateral import Quadrilateral
from rectangle import Rectangle
from square import Square

def main():
    p1 = TwoDPoint(1,1)
    p2 = TwoDPoint(1,-1)

    print(p1 - p2)
    print("--------------------")

    quad1 = Quadrilateral(5,2,4,0,-1,0,1,2)

    print(quad1.side_lengths())
    print(quad1.smallest_x())
    print(quad1)

    print("----------------Alpha")

    rect1 = Rectangle(4,4,4,0,0,0,0,4)
    Rectangle(0, 0, 0, -4, -4, -4, -4, 0)
    print(rect1.center())

    print("Testing squares")
    s1 = Square(2,2,2,0,0,0,0,2)
    s2 = Square(2,2,2,0,0,0,0,2)

    quad2 = Quadrilateral(0,0,0,-4,-4,-4,-4,0)
    quad3 = Quadrilateral(0,0,0,-4,-4,-4,-4,0)
    quad4 = Quadrilateral(0,1,2,3,4,5,6,7)
    print(quad3 == quad2)
    print(quad3 == quad4)

    print("Testing snap")
    s3 = Square(0.2,0.2,0.2,0,0,0,0,0.2)
    print(s3.center())
    print(s3.side_lengths())
    snaps3 = s3.snap()
    print(snaps3)
    print(isinstance(s3, Square))

    quad5 = Quadrilateral(4,4,4,0,0,0,0,4)
    print(rect1 == quad5)



if __name__ == "__main__":
    main()