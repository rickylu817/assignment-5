from unittest import TestCase
from two_d_point import TwoDPoint


class TestTwoDPoint(TestCase):
    def test__eq__(self):
        self.assertTrue(TwoDPoint(0, 1).__eq__(TwoDPoint(0, 1)))  # TRUE
        self.assertFalse(TwoDPoint(-4, 43).__eq__(TwoDPoint(-2324, 43434)))  # FALSE

    def test__ne__(self):
        self.assertFalse(TwoDPoint(0, 1).__ne__(TwoDPoint(0, 1)))  # FALSE
        self.assertTrue(TwoDPoint(-4, 43).__ne__(TwoDPoint(-2324, 43434)))  # TRUE

    def test__str__(self):
        self.assertEqual(TwoDPoint(4, 4).__str__(), "(4, 4)")
        self.assertNotEqual(TwoDPoint(-33434, 3343).__str__(), "(343343, 123333)")

    def test_add__(self):
        self.assertTrue(TwoDPoint(-2200, 4444).__add__(TwoDPoint(-343444, 555555)).__eq__(TwoDPoint(-345644, 559999)))
        self.assertFalse(TwoDPoint(-43, 904).__add__(TwoDPoint(3444, 565)).__eq__(TwoDPoint(3401, 1434)))

    def test_sub__(self):
        self.assertTrue(TwoDPoint(233, 76).__sub__(TwoDPoint(45, 90)).__eq__(TwoDPoint(188, -14)))
        self.assertFalse(TwoDPoint(-43, 3404).__sub__(TwoDPoint(-50, 5545)).__eq__(TwoDPoint(-93, -2141)))
