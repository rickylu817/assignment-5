from typing import List
import math

class TwoDPoint:

    def __init__(self, x, y) -> None:
        self.__x = x
        self.__y = y

    @property
    def x(self):
        return self.__x

    @property
    def y(self):
        return self.__y

    def __eq__(self, other: object) -> bool:
        other_x = other.x
        other_y = other.y

        if self.x == other_x and self.y == other_y:
            return True
        else:
            return False

    def __ne__(self, other: object) -> bool:
        return not self.__eq__(other)

    def __str__(self) -> str:
        return '(%g, %g)' % (self.__x, self.__y)

    # TODO: add magic methods such that two TwoDPoint objects can be added and subtracted coordinate-wise just by using
    #  syntax of the form p + q or p - q. New point
    def __add__(self, other: object):

        if type(self) is type(other):
            other_x = other.x
            other_y = other.y

            output = TwoDPoint(self.x + other_x, self.y + other_y)
        else:
            raise TypeError("Wrong type to add")

        return output

    def __sub__(self, other):

        if type(self) is type(other):
            other_x = other.x
            other_y = other.y

            output = TwoDPoint(self.x - other_x, self.y - other_y)
        else:
            raise TypeError("Wrong type to subtract")

        return output

    @staticmethod
    def length(point1, point2):
        point1_x = point1.x
        point1_y = point1.y

        point2_x = point2.x
        point2_y = point2.y

        output = math.sqrt(math.pow(point1_x - point2_x, 2) + math.pow(point1_y - point2_y, 2))

        return output

    @staticmethod
    def from_coordinates(coordinates: List[float]):
        if len(coordinates) % 2 != 0:
            raise Exception("Odd number of floats given to build a list of 2-d points")
        points = []
        it = iter(coordinates)
        for x in it:
            points.append(TwoDPoint(x, next(it)))

        return points
