from quadrilateral import Quadrilateral
from rectangle import Rectangle
from square import Square

class ShapeSorter:

    @staticmethod
    def sort(*args):
        output = list(args)

        # Sorts the shapes based on their smallest x-value
        # Use selection sort to write the sorting algorithm
        for i in range(0, len(output)):

            # We assume the shape that we are on right now have the smallest x-value
            # Until we have looked through the rest
            least = i

            for j in range(i, len(output)):

                # This means that the smallest x we have right now is not the smallest x
                # Hence we switch it
                if output[least].smallest_x() > output[j].smallest_x():
                    least = j

            # Now if we are out of the inner for loop that means least have the index of the smallest x-value shape
            # We just have to swap it with the shape in i
            temp = output[i]
            output[i] = output[least]
            output[least] = temp

        # If we are outside it is done sorting return the original args
        return output

def main():
    list_shapes = []
    list_shapes.append(Rectangle(0,0,0,-4,-4,-4,-4,0))
    list_shapes.append(Square(2,2,2,1,1,1,1,2))
    list_shapes.append(Quadrilateral(5,2,3,0,0,0,1.5,2))
    list_shapes.append(Quadrilateral(4,4,0,3,4,1,0,0))
    list_shapes.append(Square(2,0,0,0,0,-2,2,-2))
    list_shapes.append(Rectangle(0,0,-4,0,-4,-2,0,-2))
    list_shapes.append(Square(1,1,0,1,0,0,1,0))
    list_shapes.append(Square(5,5,0,5,0,0,5,0))
    list_shapes.append(Quadrilateral(-1,2,3,0,0,0,-5,2))

    list_shapes = ShapeSorter.sort(*list_shapes)
    for shape in list_shapes:
        print(shape)


if __name__ == "__main__":
    main()
