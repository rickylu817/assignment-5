from unittest import TestCase
from two_d_point import TwoDPoint
from rectangle import Rectangle


class TestRectangle(TestCase):
    def test__eq__(self):
        # TRUE
        self.assertTrue(Rectangle(0, 0, -4, 0, -4, -2, 0, -2).__eq__(Rectangle(0, 0, -4, 0, -4, -2, 0, -2)))
        # FALSE
        self.assertFalse(Rectangle(0, 0, -4, 0, -4, -2, 0, -2).__eq__(Rectangle(0, 0, 0, -4, -4, -4, -4, 0)))

    def test__ne__(self):
        # FALSE
        self.assertFalse(Rectangle(0, 0, -4, 0, -4, -2, 0, -2).__ne__(Rectangle(0, 0, -4, 0, -4, -2, 0, -2)))
        # TRUE
        self.assertTrue(Rectangle(0, 0, -4, 0, -4, -2, 0, -2).__ne__(Rectangle(0, 0, 0, -4, -4, -4, -4, 0)))

    def test__is_member(self):
        self.assertRaises(TypeError, lambda: Rectangle(5, 2, 3, 0, 0, 0, 1.5, 2))
        self.assertRaises(TypeError, lambda: Rectangle(-1, 2, 3, 0, 0, 0, -5, 2))

    def test_center(self):
        self.assertEqual(Rectangle(0, 0, 0, -4, -4, -4, -4, 0).center(), TwoDPoint(-2.0, -2.0))
        self.assertNotEqual(Rectangle(0, 0, -4, 0, -4, -2, 0, -2).center(), TwoDPoint(-2.0, -1.5))

    def test_area(self):
        self.assertEqual(Rectangle(0, 0, 0, -4, -4, -4, -4, 0).area(), 16.0)
        self.assertNotEqual(Rectangle(0, 0, -4, 0, -4, -2, 0, -2).area(), 8.33)
