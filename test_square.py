from unittest import TestCase
from quadrilateral import Quadrilateral
from square import Square
from two_d_point import TwoDPoint


class TestSquare(TestCase):
    def test__eq__(self):
        # TRUE
        self.assertTrue(Square(2, 2, 2, 1, 1, 1, 1, 2).__eq__(Square(2, 2, 2, 1, 1, 1, 1, 2)))
        # FALSE
        self.assertFalse(Square(2, 0, 0, 0, 0, -2, 2, -2).__eq__(Square(2, 2, 2, 1, 1, 1, 1, 2)))

    def test__ne__(self):
        # FALSE
        self.assertFalse(Square(2, 2, 2, 1, 1, 1, 1, 2).__ne__(Square(2, 2, 2, 1, 1, 1, 1, 2)))
        # TRUE
        self.assertTrue(Square(2, 0, 0, 0, 0, -2, 2, -2).__ne__(Square(2, 2, 2, 1, 1, 1, 1, 2)))

    def test__is_member(self):
        self.assertRaises(TypeError, lambda: Square(5, 2, 3, 0, 0, 0, 1.5, 2))
        self.assertRaises(TypeError, lambda: Square(-1, 2, 3, 0, 0, 0, -5, 2))

    def test_snap(self):
        # Fail to Snap
        self.assertEqual(Square(1.25, 5.3, 0.95, 5.3, 0.95, 5, 1.25, 5).snap(),
                         Square(1.25, 5.3, 0.95, 5.3, 0.95, 5, 1.25, 5))
        # Snapped
        self.assertEqual(Square(1.4, 4.2, 0, 4.2, 0, 2.8, 1.4, 2.8).snap(),
                         Quadrilateral(1, 4, 0, 4, 0, 3, 1, 3))
