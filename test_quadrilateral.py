from unittest import TestCase
from quadrilateral import Quadrilateral
import math


class TestQuadrilateral(TestCase):
    def test__eq__(self):
        # TRUE
        self.assertTrue(Quadrilateral(-1, 2, 3, 0, 0, 0, -5, 2).__eq__(Quadrilateral(-1, 2, 3, 0, 0, 0, -5, 2)))
        # FALSE
        self.assertFalse(Quadrilateral(5, 2, 3, 0, 0, 0, 1.5, 2).__eq__(Quadrilateral(-1, 2, 3, 0, 0, 0, -5, 2)))

    def test__ne__(self):
        # FALSE
        self.assertFalse(Quadrilateral(-1, 2, 3, 0, 0, 0, -5, 2).__ne__(Quadrilateral(-1, 2, 3, 0, 0, 0, -5, 2)))
        # TRUE
        self.assertTrue(Quadrilateral(5, 2, 3, 0, 0, 0, 1.5, 2).__ne__(Quadrilateral(-1, 2, 3, 0, 0, 0, -5, 2)))

    def test_side_lengths(self):
        self.assertEqual(Quadrilateral(4, 4, 0, 3, 0, 0, 4, 1).side_lengths(), (math.sqrt(17), 3, math.sqrt(17), 3))
        self.assertNotEqual(Quadrilateral(-1, 2, 3, 0, 0, 0, -5, 2).side_lengths(), (1, 3, 2, 3))

    def test_smallest_x(self):
        self.assertEqual(Quadrilateral(5, 2, 3, 0, 0, 0, 1.5, 2).smallest_x(), 0)
        self.assertNotEqual(Quadrilateral(-5, 2, -23, 0, -30, 0, 1, 2).smallest_x(), 23)
